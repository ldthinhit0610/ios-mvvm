//
//  RepoModel.swift
//  iOS-MVVM-Template
//
//  Created by Luu Duc Thinh on 16/11/2020.
//

import Foundation
import ObjectMapper

struct RepoModel : Mappable{
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        full_name <- map["full_name"]
        description <- map["description"]
        
    }
    
    var full_name = ""
    var description = ""
    
}
