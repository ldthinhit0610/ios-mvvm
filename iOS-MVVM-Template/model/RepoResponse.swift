//
//  RepoResponse.swift
//  iOS-MVVM-Template
//
//  Created by revu_macair on 11/17/20.
//

import Foundation
import ObjectMapper

struct RepoResponse : Mappable{
    var message = ""
    var items : [RepoModel]?
    var errors : [Any]?

    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        items <- map["items"]
        message <- map["message"]
        errors <- map["errors"]
    }
    
    
}
