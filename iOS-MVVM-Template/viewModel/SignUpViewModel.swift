//
//  SignUpViewModel.swift
//  iOS-MVVM-Template
//
//  Created by revu_macair on 11/16/20.
//

import Foundation
import RxSwift
import RxCocoa

class SignUpViewModel {
    private var disposeBag: DisposeBag = DisposeBag()
    private var email = String()
    //for input
    var emailInputTextChange : AnyObserver<String?>
    var btnClickInput : AnyObserver<Void?>

    //for output
    var isButtonEnable: Driver<Bool>
    var statusSignUp: Driver<UserModel?>
    
    //
    private let emailEventSubject: BehaviorSubject<String?>
    private let loginBtnClick: PublishSubject<Void?>
    private let apiResponseSubject = PublishSubject<UserModel?>()
    
    
    //for service
    private lazy var userService = UserService()
    
    init() {
        let emailInputSubject = BehaviorSubject<String?>(value: "")
        self.emailEventSubject = emailInputSubject
        emailInputTextChange = emailEventSubject.asObserver()
        let btnClickSubject = PublishSubject<Void?>()
        self.loginBtnClick = btnClickSubject
        btnClickInput = btnClickSubject.asObserver()

        isButtonEnable = emailInputSubject.map({ (email) -> Bool in
            return email?.count ?? 0 > 10
        }).asDriver(onErrorJustReturn: false)
        statusSignUp = apiResponseSubject.asDriver(onErrorJustReturn: nil)
        
        initInputEvent()
    }
    
    func initInputEvent() {
        emailEventSubject.subscribe(onNext: { [weak self] email in
            self?.email = email ?? ""
        }).disposed(by: disposeBag)
        
        loginBtnClick.throttle(.milliseconds(500), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.requestSignUp()
            })
            .disposed(by: disposeBag)
    }
    
    
    func requestSignUp() {
        apiResponseSubject.onNext(userService.requestSignIn(email: self.email))
    }
}
