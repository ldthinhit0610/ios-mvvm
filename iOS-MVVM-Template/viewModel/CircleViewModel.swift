//
//  CircleViewModel.swift
//  iOS-MVVM-Template
//
//  Created by Luu Duc Thinh on 16/11/2020.
//
//link https://viblo.asia/p/rxswift-qua-vi-du-2-observable-va-the-bind-4dbZNExQKYM

import Foundation
import RxSwift
import RxCocoa

class CircleViewModel {
    var centerVariable = BehaviorSubject<CGPoint?>(value: .zero) // Create one variable that will be changed and observed
    var backgroundColorObservable: Observable<UIColor>! // Create observable that will change backgroundColor based on center
    
    init() {
        setup()
    }
    
    func setup() {
        // When we get new center, emit new UIColor
        backgroundColorObservable = centerVariable.asObservable()
            .map { center in
                guard let center = center else { return UIColor.black }
                
                
                let red: CGFloat = (center.x + center.y).truncatingRemainder(dividingBy: 255.0) / 255.0 // We just manipulate red, but you can do w/e
                let green: CGFloat = 0.0
                let blue: CGFloat = 0.0
                return UIColor.init(red: red, green: green, blue: blue, alpha: 1.0)
            }
    }
}
