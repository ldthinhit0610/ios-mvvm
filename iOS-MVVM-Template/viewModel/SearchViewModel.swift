//
//  SearchViewModel.swift
//  iOS-MVVM-Template
//
//  Created by Luu Duc Thinh on 16/11/2020.
//

import Foundation
import RxSwift
import RxCocoa

class SearchViewModel {
    private var disposeBag: DisposeBag = DisposeBag()
    private lazy var searchService = RepoService()
    //input
    let inputTextChange = BehaviorSubject<String?>(value: "")
    //output
    let searchRepoResponse = PublishSubject<[RepoModel]?>()
    init() {
        initInputEvent()
    }
    
    func initInputEvent() {
        inputTextChange
            .debounce(.milliseconds(800), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { (value) in
                self.requestSearch(key: value ?? "")
            }).disposed(by: disposeBag)
    }
    
    func requestSearch(key : String) {
        if key.isEmpty {
            searchRepoResponse.onNext([])
        }else{
            searchService.getRepoByName(key: key)
                .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] (list) in
                    self?.searchRepoResponse.onNext(list)
                }, onError: { (error) in
                    print(error.localizedDescription)
                } ,onCompleted: {
                    print("onCompleted:::::")
                }).disposed(by: disposeBag)
            
        }
    }
}
