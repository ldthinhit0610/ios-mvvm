//
//  RepoService.swift
//  iOS-MVVM-Template
//
//  Created by revu_macair on 11/17/20.
//

import Foundation
import RxSwift
import ObjectMapper

class RepoService {
    func getRepoByName(key : String) -> Observable<[RepoModel]?>{
        return Network.rxRequest(url: "https://api.github.com/search/repositories", params : ["q": key]).map { (response) -> [RepoModel]? in
            return Mapper<RepoResponse>().map(JSONObject: response)?.items
        }
    }
}
