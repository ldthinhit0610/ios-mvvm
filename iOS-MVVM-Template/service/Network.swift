//
//  Network.swift
//  iOS-MVVM-Template
//
//  Created by revu_macair on 11/17/20.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift

class Network: NSObject {
    static let share = Network()
    
    private override init() {
        
    }
    
    class func rxRequest(_ method:HTTPMethod = .get, url:String, params:[String: Any]? = nil) -> Observable<Any> {
        return Observable.create { observer -> Disposable in
            
            var encType : ParameterEncoding = URLEncoding(destination: .queryString)
            
            if method == HTTPMethod.post || method == HTTPMethod.post || method == HTTPMethod.post {
                encType = JSONEncoding.prettyPrinted
            }
            
            print("Request  : \(method), \(url)")
            
            Alamofire.request(url, method: method, parameters: params, encoding:encType, headers:nil)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let obj):
                        guard response.data != nil else {
                            observer.onError(response.error!)
                            return
                        }
                        
                        print("Response : \(url) \n\(JSON(obj))")
                        observer.onNext(obj)
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode
                        {
                            observer.onError(NSError(domain: "\(url)", code: statusCode, userInfo: nil))
                        }
                        observer.onError(error)
                    }
                }
            
            return Disposables.create()
        }
    }
}
