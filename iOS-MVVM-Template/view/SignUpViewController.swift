//
//  SignUpViewController.swift
//  iOS-MVVM-Template
//
//  Created by revu_macair on 11/16/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController {
    var circleView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    var disposeBag = DisposeBag()
    lazy var signUpViewModel = SignUpViewModel()
    lazy var circleViewModel = CircleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        bindViewModel()
        
        setup()
    }
    
    func initUI() {
        
    }
    
    func bindViewModel() {
        //bind View -> VM
        emailTextField.rx.text.bind(to: signUpViewModel.emailInputTextChange).disposed(by: disposeBag)
        btnRegister.rx.tap.bind(to: signUpViewModel.btnClickInput).disposed(by: disposeBag)
        
        //observer from VM
        signUpViewModel.isButtonEnable.drive(onNext: { [weak self] (isEnable) in
            self?.btnRegister.isEnabled = isEnable
        }).disposed(by: disposeBag)
        
        //for network
        signUpViewModel.statusSignUp.drive(onNext: { (userModel) in
            if let response = userModel{
                print("GOTO HOME \(response)")
            }else{
                print("ERROR")
            }
        }).disposed(by: disposeBag)
        
        
    }
    
    func setup() {
        // Add circle view
        circleView = UIView(frame: CGRect(origin: view.center, size: CGSize(width: 100.0, height: 100.0)))
        circleView.layer.cornerRadius = circleView.frame.width / 2.0
        circleView.center = view.center
        circleView.backgroundColor = .green
        view.addSubview(circleView)
        
        circleView
            .rx.observe(CGPoint.self, "center")
            .bind(to: circleViewModel.centerVariable)
            .disposed(by: disposeBag)
        circleViewModel.backgroundColorObservable
            .subscribe(onNext: { [weak self] backgroundColor in
                UIView.animate(withDuration: 0.1) {
                    self?.circleView.backgroundColor = backgroundColor
                }
            })
            .disposed(by: disposeBag)
        // Add gesture recognizer
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(circleMoved(_:)))
        circleView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func circleMoved(_ recognizer: UIPanGestureRecognizer) {
        let location = recognizer.location(in: view)
        UIView.animate(withDuration: 0.1) {
            self.circleView.center = location
        }
    }
}
