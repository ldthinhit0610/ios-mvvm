//
//  SearchViewController.swift
//  iOS-MVVM-Template
//
//  Created by Luu Duc Thinh on 16/11/2020.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class SearchViewController: UIViewController, UITableViewDataSource {
    let CellIdentifier = "com.codepath.MyFirstTableViewCell"
    private var disposeBag: DisposeBag = DisposeBag()
    private var repoItems : [RepoModel] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repoItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        cell.textLabel?.text = repoItems[indexPath.row].full_name
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    lazy var searchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        
        searchTextField.rx.text.bind(to: searchViewModel.inputTextChange).disposed(by: disposeBag)
        searchViewModel.searchRepoResponse.subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { (repoList) in
                self.repoItems = repoList ?? []
            })
            .disposed(by: disposeBag)
        
    }
}
